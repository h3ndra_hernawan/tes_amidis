<?php

namespace App;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class BarangHeaders extends Model
{
    use Notifiable;
    // public $timestamps = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'barang_headers';
    protected $primaryKey = 'id';
    protected $fillable = [
            'no_permintaan',
            'no_ktp',
            'tgl_permintaan', 
            'id_user_login',
            'total'
        ];
}
