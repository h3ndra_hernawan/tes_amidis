<?php

namespace App;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class Barangdetails extends Model
{
    use Notifiable;
    // public $timestamps = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'barang_details';
    protected $primaryKey = 'id';
    protected $fillable = [
            'no_permintaan',
            'id_barang_header',
            'no_ktp', 
            'id_master_barang',
            'kuantiti',
            'keterangan',
            'id_user_login'
        ];
}
