<?php

namespace App\Http\Controllers;

use App\MasterBarangs;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Redirect,Response;
use DataTables;
use Validator;
use DB;
use Alert;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;

class MasterBarangController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
     
        if ($request->ajax()) {
             $data = MasterBarangs::latest('id')->get();
            
            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){
                         $btn = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row['id'].'" data-original-title="Edit" class="edit btn btn-primary btn-sm editItem"><i class="fas fa-user-edit"></i>  Edit</a>';
                         $btn = $btn.' <a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row['id'].'" data-original-title="Delete" class="btn btn-danger btn-sm deleteItem"><i class="fas fa-user-times"></i>  Delete</a>';
                        
                        return $btn;
                    })
                    ->rawColumns(['action'])
                    ->make(true);
        }
        $MasterBarang = 'BRG-'.date('dHis')."/MSR-".date('m').'/'.date('Y'); 
        $page = "MasterBarang";
        return view('MasterBarang.index',compact('page','MasterBarang'));
    }
 
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }
 
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      
        $validator = Validator::make($request->all(), [
            'kode_barang' => 'required',
            'nama_barang' => 'required',
            'lokasi_barang' => 'required',
            'stok' => 'required',
            'satuan' => 'required',
            'harga_barang' => 'required'
        ]);
        if ($validator->passes()) {
            $post = MasterBarangs::updateOrCreate(
                 ['id' => $request->id], 
                 ['kode_barang' =>  $request->kode_barang,
                'nama_barang' => $request->nama_barang,
                'lokasi_barang' => $request->lokasi_barang,
                'stok' => $request->stok,
                'satuan' => $request->satuan,
                'harga_barang' =>(double)str_replace(',','',strval($request->harga_barang)),
                'id_user_login' => session()->get('userid'),
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ]);
             if(!is_null($post)) {  
                 $response = array('status' => 'success', 'error' => '', /*'emr_igd_triase' => $dosave,*/  'message' => 'Data Berhasil Di Simpan');      
             }else{     
                 $response = array('status' => 'error', 'message' => 'Tambah data Gagal');
             }
        }else{
            return response()->json(['error'=>$validator->errors()->all()]);
        }
    
        echo json_encode($response);
        
    }
 
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = MasterBarangs::find($id);
        return response()->json($user);
    }
 
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = MasterBarangs::find($id);
        return response()->json($user);
    }
 
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }
 
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
 
        $delete = MasterBarangs::find($id)->delete();
 
        // check data deleted or not
        if ($delete == 1) {
            $success = true;
            $response = array('status' => 'success', 'error' => '', /*'emr_igd_triase' => $dosave,*/  'message' => 'Hapus data berhasil');      
        } else {
            $success = true;
            $response = array('status' => 'error', 'message' => 'Hapus data Gagal');
        }
 
        //  Return response
        echo json_encode($response);
    }
}
