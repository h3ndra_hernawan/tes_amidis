<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use DateTime;
use Illuminate\Support\Facades\Auth;
use Validator;
use DataTables;
use Hash;
use Session;
use Haruncpi\LaravelIdGenerator\IdGenerator;
use App\MasterKaryawans;
use App\MasterBarangs;
use App\BarangHeaders;
use App\Barangdetails;
use PDF;

class PermintaanBarangController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $new_date = date('dHis')."/PRN/BRG-PERMINTAAN/".date('m').'/'.date('Y'); 
        $MasterBarang = MasterBarangs::get(); 
        $MasterKaryawan = MasterKaryawans::get();  
   
        $page = 'PermintaanBarang';
        return view('PermintaanBarang.index',compact('page','MasterBarang','MasterKaryawan','new_date'));
    }

    public function getPermintaanBarang(Request $request)
    {
      
        if ($request->ajax()) {
            if (session()->get('level') == 1){
                $DataHeader = BarangHeaders::select('barang_headers.no_permintaan','barang_headers.no_ktp','master_karyawans.nama','master_karyawans.departemen','barang_headers.total','barang_headers.tgl_permintaan')
                ->leftJoin('master_karyawans', function($join) {
                    $join->on('barang_headers.no_ktp', '=', 'master_karyawans.no_ktp');
                })
                ->get(); 
            }else{
                $DataHeader = BarangHeaders::select('barang_headers.no_permintaan','barang_headers.no_ktp','master_karyawans.nama','master_karyawans.departemen','barang_headers.total','barang_headers.tgl_permintaan')
                ->leftJoin('master_karyawans', function($join) {
                    $join->on('barang_headers.no_ktp', '=', 'master_karyawans.no_ktp');
                })
                ->where('barang_headers.id_user_login',session()->get('userid') )
                ->get(); 
                
            }
         
            return Datatables::of($DataHeader)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){
                        $Encrypted = base64_encode($row['no_permintaan']);
                        $btn = '<a href="javascript:void(0)" data-id="'.$row['no_permintaan'].'" data-no_ktp="'.$row['no_ktp'].'"  class="btn btn-outline-success btn-md getDetail"><i class="icon-check"></i> Detail</a>';
                        $btn = $btn.' <a href="data/print/'.$Encrypted.'" class="btn btn-outline-primary btn-md" target="_blank"><i class="icon-printer"></i> Print</a>';
                        
                        return $btn;
                    })
                    ->rawColumns(['action'])
                    ->escapeColumns([])
                    ->make(true);
        }
   
        $page = 'PermintaanBarang';
        return view('PermintaanBarang.index',compact('page'));
    }
    public function getPermintaanBarangDetail(Request $request)
    {
        // dd($request->all());
        if ($request->ajax()) {
            $DataHeader = Barangdetails::select('barang_details.no_permintaan','master_karyawans.no_ktp','master_karyawans.nama','master_karyawans.departemen','barang_headers.tgl_permintaan',
            'master_barangs.nama_barang','master_barangs.lokasi_barang','master_barangs.stok','barang_details.kuantiti','master_barangs.satuan','barang_details.keterangan','master_barangs.harga_barang')
            ->leftJoin('barang_headers', function($join) {
                $join->on('barang_details.no_permintaan', '=', 'barang_headers.no_permintaan');
            })
            ->leftJoin('master_karyawans', function($join) {
                $join->on('barang_headers.no_ktp', '=', 'master_karyawans.no_ktp');
            })
            ->leftJoin('master_barangs', function($join) {
                $join->on('barang_details.kode_master_barang', '=', 'master_barangs.kode_barang');
            })
            ->where('barang_details.no_permintaan',$request->no_permintaan)
            ->get(); 
           
        //  var_dump($DataHeader);
        //  die();
            return Datatables::of($DataHeader)
                    ->addIndexColumn()
                    ->escapeColumns([])
                    ->make(true);
        }
   
        // $page = 'PermintaanBarang';
        // return view('PermintaanBarang.index',compact('page'));
    }
    public function gettotalPermintaanBarangDetail(Request $request){
        // dd($request->all());
        $BarangHeaders = BarangHeaders::where('no_permintaan',$request->no_permintaan)->get();
        return response()->json($BarangHeaders);
    }
    public function masterbarang(Request $request)
    {
        $rek=  MasterBarangs::select("kode_barang || ' (' || nama_barang || ')' as nama,kode_barang,nama_barang,lokasi_barang,stok,satuan,harga_barang")
        ->get(); 
        return  $rek;
        
    }

    public function print_pdf($id){
        $id = base64_decode($id);
        $DataDetail = Barangdetails::select('barang_details.no_permintaan','master_karyawans.no_ktp','master_karyawans.nama','master_karyawans.departemen','barang_headers.tgl_permintaan',
            'master_barangs.nama_barang','master_barangs.lokasi_barang','master_barangs.stok','barang_details.kuantiti','master_barangs.satuan','barang_details.keterangan','master_barangs.harga_barang')
            ->leftJoin('barang_headers', function($join) {
                $join->on('barang_details.no_permintaan', '=', 'barang_headers.no_permintaan');
            })
            ->leftJoin('master_karyawans', function($join) {
                $join->on('barang_headers.no_ktp', '=', 'master_karyawans.no_ktp');
            })
            ->leftJoin('master_barangs', function($join) {
                $join->on('barang_details.kode_master_barang', '=', 'master_barangs.kode_barang');
            })
            ->where('barang_details.no_permintaan',$id)
            ->get(); 

        $DataHeader = BarangHeaders::select('barang_headers.no_permintaan','barang_headers.no_ktp','master_karyawans.nama','master_karyawans.departemen','barang_headers.total','barang_headers.tgl_permintaan')
            ->leftJoin('master_karyawans', function($join) {
                $join->on('barang_headers.no_ktp', '=', 'master_karyawans.no_ktp');
            })
            ->where('barang_headers.no_permintaan', '=',$id)->first();
    
        $pdf = PDF::loadView('PermintaanBarang.print_pdf', compact('DataDetail','DataHeader'))->setPaper('A4', 'landscape');
        return $pdf->stream('Laporan_Permintaan_Barang.pdf');
       
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        $this->validate($request, [
            'no_ktp' => 'required',
            'tgl_permintaan' => 'required',
            'retribusi.*.kode_barang_input' => 'required',
            'retribusi.*.kuantiti' => 'required',
            'retribusi.*.keterangan' => 'required',
        ]);

        $total=0;
        foreach ($request->retribusi as $items => $item) {
            $detail = DB::table('barang_details')->insert([
                'no_permintaan' => $request->no_permintaan,
                'no_ktp' => (int)$request->no_ktp,
                'kode_master_barang' => $item['kode_barang_input'],
                'kuantiti' => $item['kuantiti'],
                'keterangan' => $item['keterangan'], 
                'id_user_login' => session()->get('userid'),
                'created_at' =>date('Y-m-d H:i:s')

            ]);
        
            $total +=(double)str_replace(',','',strval($item['harga_barang']));

            if(is_null($detail)) {            
                $response = array('status' => 'error', 'message' => 'Detail  Gagal Tersimpan');
            }

            // get data master barang 
            $MasterBarang = MasterBarangs::select('kode_barang','stok')
            ->where('kode_barang',$item['kode_barang_input'])->first();
            
            $updateMasterBarang = DB::table('master_barangs')->where('kode_barang',$item['kode_barang_input'])->update([
                'stok' => $MasterBarang['stok'] - $item['kuantiti'] ,
                'id_user_login' => session()->get('userid'),
                'updated_at' =>date('Y-m-d H:i:s')
            ]);
            if(is_null($updateMasterBarang)) {            
                $response = array('status' => 'error', 'message' => 'Master Barang Gagal Update Stok');
            }
        }

        $header = DB::table('barang_headers')->insert([
            'no_permintaan' => $request->no_permintaan,
            'no_ktp' => (int)$request->no_ktp,
            'tgl_permintaan' => date('Y-m-d', strtotime($request->tgl_permintaan)),
            'id_user_login' => session()->get('userid'), 
            'total' => $total,
            'created_at' =>date('Y-m-d H:i:s')

        ]);
        if(!is_null($header)) {   
            $response = array('status' => 'success', 'error' => '', /*'emr_igd_triase' => $dosave,*/  'message' => 'Tambah data detail berhasil');   
        }    
        else {
            $response = array('status' => 'error', 'message' => 'Data  Gagal Tersimpan');
        }
        echo json_encode($response);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
