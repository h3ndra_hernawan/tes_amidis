<?php

namespace App;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class MasterBarangs extends Model
{
    use Notifiable;
    // public $timestamps = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'master_barangs';
    protected $primaryKey = 'id';
    protected $fillable = [
            'kode_barang',
            'nama_barang', 
            'lokasi_barang',
            'stok',
            'satuan',
            'harga_barang',
            'id_user_login'
        ];
}
