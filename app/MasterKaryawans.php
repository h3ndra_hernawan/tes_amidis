<?php

namespace App;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class MasterKaryawans extends Model
{
    use Notifiable;
    // public $timestamps = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'master_karyawans';
    protected $primaryKey = 'id';
    protected $fillable = [
            'no_ktp',
            'nama', 
            'departemen',
            'id_user_login'
        ];
}
