<?php
namespace App\Observers;

use App\Notifications\NewItem;
use App\SetoranHeader;
use App\User;

class ItemObserver
{
    public function created(SetoranHeader $setoranheader)
    {
        $author = $setoranheader->user;
        $users = User::all();
        foreach ($users as $user) {
            $user->notify(new NewItem($setoranheader,$author));
        }
    }
}