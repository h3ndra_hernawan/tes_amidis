<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\BarangHeaders;
use Faker\Generator as Faker;

$factory->define(BarangHeaders::class, function (Faker $faker) {
    return [
        'no_permintaan' => $faker->no_permintaan,
        'no_ktp' => $faker->no_ktp,
        'tgl_permintaan' => $faker->tgl_permintaan,
        'id_user_login' => $faker->id_user_login,
        'total' => $faker->total,
        'created_at' => now(),
    ];
});
