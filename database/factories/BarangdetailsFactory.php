<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Barangdetails;
use Faker\Generator as Faker;

$factory->define(Barangdetails::class, function (Faker $faker) {
    return [
        'no_permintaan' => $faker->no_permintaan,
        'no_ktp' => $faker->no_ktp,
        'kode_master_barang' => $faker->kode_master_barang,
        'kuantiti' => $faker->kuantiti,
        'keterangan' => $faker->keterangan,
        'id_user_login' => $faker->id_user_login,
        'created_at' => now(),
    ];
});
