<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\MasterBarangs;
use Faker\Generator as Faker;

$factory->define(MasterBarangs::class, function (Faker $faker) {
    return [
        'kode_barang' => $faker->kode_barang,
        'nama_barang' => $faker->nama_barang,
        'lokasi_barang' => $faker->lokasi_barang,
        'stok' => $faker->stok,
        'satuan' => $faker->satuan,
        'harga_barang' => $faker->harga_barang,
        'id_user_login' => $faker->id_user_login,
        'created_at' => now(),
    ];
});
