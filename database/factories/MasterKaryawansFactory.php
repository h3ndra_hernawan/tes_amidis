<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\MasterKaryawans;
use Faker\Generator as Faker;

$factory->define(MasterKaryawans::class, function (Faker $faker) {
    return [
        'no_ktp' => $faker->no_ktp,
        'nama' => $faker->nama,
        'departemen' => $faker->departemen,
        'id_user_login' => $faker->id_user_login,
        'created_at' => now(),
    ];
});
