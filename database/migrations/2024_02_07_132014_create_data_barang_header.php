<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDataBarangHeader extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('barang_headers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('no_permintaan')->unique(); 
            $table->integer('no_ktp');
            $table->timestamp('tgl_permintaan');
            $table->string('id_user_login');      
            $table->decimal('total', 12, 2);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('barang_headers');
    }
}
