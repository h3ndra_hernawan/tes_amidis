<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDataBarangDetail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('barang_details', function (Blueprint $table) {
            $table->bigIncrements('id'); 
            $table->string('no_permintaan');
            $table->integer('no_ktp');
            $table->string('kode_master_barang');
            $table->integer('kuantiti');
            $table->string('keterangan');
            $table->string('id_user_login');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('barang_details');
    }
}
