<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class BarangDetailsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\Barangdetails::factory(20)->create();
    }
}
