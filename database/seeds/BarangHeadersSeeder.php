<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class BarangHeadersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\BarangHeaders::factory(20)->create();
    }
}
