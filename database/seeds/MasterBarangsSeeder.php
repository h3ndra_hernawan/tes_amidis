<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class MasterBarangsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\MasterBarangs::factory(20)->create();
    }
}
