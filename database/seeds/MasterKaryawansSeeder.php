<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class MasterKaryawansSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\MasterKaryawans::factory(20)->create();
    }
}
