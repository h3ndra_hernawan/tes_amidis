-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 08 Feb 2024 pada 18.52
-- Versi server: 10.4.6-MariaDB
-- Versi PHP: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `amidis_tes`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `barang_details`
--

CREATE TABLE `barang_details` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `no_permintaan` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `no_ktp` int(11) NOT NULL,
  `kode_master_barang` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kuantiti` int(11) NOT NULL,
  `keterangan` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_user_login` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `barang_details`
--

INSERT INTO `barang_details` (`id`, `no_permintaan`, `no_ktp`, `kode_master_barang`, `kuantiti`, `keterangan`, `id_user_login`, `created_at`, `updated_at`) VALUES
(1, '08174555/PRN/BRG-PERMINTAAN/02/2024', 123453, 'BRG-08173151/MSR-02/2024', 1, 'r', '11', '2024-02-08 10:47:57', NULL),
(2, '08174555/PRN/BRG-PERMINTAAN/02/2024', 123453, 'BRG-08173528/MSR-02/2024', 1, 'r', '11', '2024-02-08 10:47:57', NULL),
(3, '08174555/PRN/BRG-PERMINTAAN/02/2024', 123453, 'BRG-08173609/MSR-02/2024', 1, 'r', '11', '2024-02-08 10:47:57', NULL),
(4, '08174555/PRN/BRG-PERMINTAAN/02/2024', 123453, 'BRG-08173938/MSR-02/2024', 1, 'r', '11', '2024-02-08 10:47:57', NULL),
(5, '08174555/PRN/BRG-PERMINTAAN/02/2024', 123453, 'BRG-08173955/MSR-02/2024', 1, 'r', '11', '2024-02-08 10:47:57', NULL),
(6, '08174555/PRN/BRG-PERMINTAAN/02/2024', 123453, 'BRG-08174011/MSR-02/2024', 1, 'r', '11', '2024-02-08 10:47:57', NULL),
(7, '08174555/PRN/BRG-PERMINTAAN/02/2024', 123453, 'BRG-08174027/MSR-02/2024', 1, 'r', '11', '2024-02-08 10:47:57', NULL),
(8, '08174555/PRN/BRG-PERMINTAAN/02/2024', 123453, 'BRG-08174125/MSR-02/2024', 1, 'r', '11', '2024-02-08 10:47:57', NULL),
(9, '08174555/PRN/BRG-PERMINTAAN/02/2024', 123453, 'BRG-08174108/MSR-02/2024', 1, 'r', '11', '2024-02-08 10:47:57', NULL),
(10, '08174555/PRN/BRG-PERMINTAAN/02/2024', 123453, 'BRG-08174125/MSR-02/2024', 1, 'r', '11', '2024-02-08 10:47:57', NULL),
(11, '08175012/PRN/BRG-PERMINTAAN/02/2024', 334422, 'BRG-08173151/MSR-02/2024', 1, 'tes', '12', '2024-02-08 10:51:41', NULL),
(12, '08175012/PRN/BRG-PERMINTAAN/02/2024', 334422, 'BRG-08173609/MSR-02/2024', 1, 'tes', '12', '2024-02-08 10:51:41', NULL),
(13, '08175012/PRN/BRG-PERMINTAAN/02/2024', 334422, 'BRG-08173938/MSR-02/2024', 1, 'ftes', '12', '2024-02-08 10:51:41', NULL),
(14, '08175012/PRN/BRG-PERMINTAAN/02/2024', 334422, 'BRG-08173955/MSR-02/2024', 1, 'ftes', '12', '2024-02-08 10:51:41', NULL),
(15, '08175012/PRN/BRG-PERMINTAAN/02/2024', 334422, 'BRG-08174125/MSR-02/2024', 1, 'tes', '12', '2024-02-08 10:51:41', NULL),
(16, '08175012/PRN/BRG-PERMINTAAN/02/2024', 334422, 'BRG-08174125/MSR-02/2024', 1, 'tes', '12', '2024-02-08 10:51:41', NULL),
(17, '08175012/PRN/BRG-PERMINTAAN/02/2024', 334422, 'BRG-08174108/MSR-02/2024', 1, 'v', '12', '2024-02-08 10:51:41', NULL),
(18, '08175012/PRN/BRG-PERMINTAAN/02/2024', 334422, 'BRG-08174050/MSR-02/2024', 1, 'tes', '12', '2024-02-08 10:51:41', NULL),
(19, '08175012/PRN/BRG-PERMINTAAN/02/2024', 334422, 'BRG-08174108/MSR-02/2024', 1, 'tes', '12', '2024-02-08 10:51:41', NULL),
(20, '08175012/PRN/BRG-PERMINTAAN/02/2024', 334422, 'BRG-08173528/MSR-02/2024', 1, 'tes', '12', '2024-02-08 10:51:41', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `barang_headers`
--

CREATE TABLE `barang_headers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `no_permintaan` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `no_ktp` int(11) NOT NULL,
  `tgl_permintaan` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `id_user_login` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `total` decimal(12,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `barang_headers`
--

INSERT INTO `barang_headers` (`id`, `no_permintaan`, `no_ktp`, `tgl_permintaan`, `id_user_login`, `total`, `created_at`, `updated_at`) VALUES
(1, '08174555/PRN/BRG-PERMINTAAN/02/2024', 123453, '2024-08-01 17:00:00', '11', '230380600.00', '2024-02-08 10:47:57', NULL),
(2, '08175012/PRN/BRG-PERMINTAAN/02/2024', 334422, '2024-08-01 17:00:00', '12', '30410600.00', '2024-02-08 10:51:41', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `master_barangs`
--

CREATE TABLE `master_barangs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `kode_barang` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama_barang` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lokasi_barang` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `stok` int(11) NOT NULL,
  `satuan` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `harga_barang` decimal(12,2) NOT NULL,
  `id_user_login` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `master_barangs`
--

INSERT INTO `master_barangs` (`id`, `kode_barang`, `nama_barang`, `lokasi_barang`, `stok`, `satuan`, `harga_barang`, `id_user_login`, `created_at`, `updated_at`) VALUES
(1, 'BRG-08173151/MSR-02/2024', 'barang 1', 'barang 1', 5, 'PCS', '20000000.00', '12', '2024-02-08 10:34:37', '2024-02-08 10:51:41'),
(4, 'BRG-08173528/MSR-02/2024', 'Laptop', 'Bandung', 18, 'PCS', '20000.00', '12', '2024-02-08 10:35:55', '2024-02-08 10:51:41'),
(5, 'BRG-08173609/MSR-02/2024', 'Laptop', 'Bandung', 18, 'LEMBAR', '300000.00', '12', '2024-02-08 10:36:36', '2024-02-08 10:51:41'),
(7, 'BRG-08173938/MSR-02/2024', 'Mecbook', 'Bandung', 18, 'PCS', '10000000.00', '12', '2024-02-08 10:39:55', '2024-02-08 10:51:41'),
(8, 'BRG-08173955/MSR-02/2024', 'tes barang', 'Calender', 18, 'LUSIN', '20000.00', '12', '2024-02-08 10:40:11', '2024-02-08 10:51:41'),
(9, 'BRG-08174011/MSR-02/2024', 'tes barang 2 edit', 'Calender', 19, 'LEMBAR', '20000.00', '11', '2024-02-08 10:40:27', '2024-02-08 10:47:57'),
(10, 'BRG-08174027/MSR-02/2024', 'tes barang 2 edit', 'barang 2', 17, 'LEMBAR', '200000000.00', '11', '2024-02-08 10:40:49', '2024-02-08 10:47:57'),
(11, 'BRG-08174050/MSR-02/2024', 'tes barang edit', 'Bandung', 17, 'LEMBAR', '30000.00', '12', '2024-02-08 10:41:08', '2024-02-08 10:51:41'),
(12, 'BRG-08174108/MSR-02/2024', 'tes barang 2 edit', 'Bandung', 17, 'PCS', '20000.00', '12', '2024-02-08 10:41:24', '2024-02-08 10:51:41'),
(13, 'BRG-08174125/MSR-02/2024', 'fff', 'fff', 16, 'PCS', '300.00', '12', '2024-02-08 10:41:49', '2024-02-08 10:51:41');

-- --------------------------------------------------------

--
-- Struktur dari tabel `master_karyawans`
--

CREATE TABLE `master_karyawans` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `no_ktp` int(11) NOT NULL,
  `nama` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `departemen` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_user_login` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `master_karyawans`
--

INSERT INTO `master_karyawans` (`id`, `no_ktp`, `nama`, `departemen`, `id_user_login`, `created_at`, `updated_at`) VALUES
(1, 123453, 'hendra', 'Senior Programmer', '11', '2024-02-08 10:43:52', '2024-02-08 10:43:52'),
(2, 222111, 'admin', 'Senior Programmer', '11', '2024-02-08 10:44:04', '2024-02-08 10:44:04'),
(6, 1234532, 'admin', 'Calendder 1', '11', '2024-02-08 10:44:38', '2024-02-08 10:44:38'),
(7, 334422, 'user', 'Calendder 1', '11', '2024-02-08 10:44:50', '2024-02-08 10:44:50'),
(8, 321234, 'hendra', 'Senior Programmer', '11', '2024-02-08 10:45:02', '2024-02-08 10:45:02'),
(9, 33442233, 'ddd', 'ddd', '11', '2024-02-08 10:45:10', '2024-02-08 10:45:10'),
(10, 4317, 'dd', 'dd', '11', '2024-02-08 10:45:17', '2024-02-08 10:45:17'),
(11, 55633, 'rr', 'rrr', '11', '2024-02-08 10:45:30', '2024-02-08 10:45:30'),
(12, 5678, 'user', 'ddd', '11', '2024-02-08 10:45:40', '2024-02-08 10:45:40'),
(13, 32113, 'nama 1', 'Senior Programmer', '11', '2024-02-08 10:45:51', '2024-02-08 10:45:51');

-- --------------------------------------------------------

--
-- Struktur dari tabel `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2024_02_07_131741_create_data_master_karyawan', 1),
(3, '2024_02_07_131841_create_data_master_barang', 1),
(4, '2024_02_07_132014_create_data_barang_header', 1),
(5, '2024_02_07_132021_create_data_barang_detail', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `level` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`id`, `user`, `email`, `name`, `email_verified_at`, `password`, `status`, `level`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'admin1', 'admin1', 'admin1', NULL, '$2y$10$L.OiZNzQn7pUH54zp.DWgeK2MKWjmlhf8Ndlc11sood0D9lmSVE7q', '1', '1', NULL, '2024-02-08 10:28:32', NULL),
(2, 'admin2', 'admin2', 'admin2', NULL, '$2y$10$AhTyGQbei4EJUQa7A/jyLuwtxUuGrAbufhQNwcaSUQlUPdvHT60Lm', '1', '1', NULL, '2024-02-08 10:28:45', NULL),
(3, 'admin3', 'admin3', 'admin3', NULL, '$2y$10$MFcvmyIYFdCnK2fhLCJ2rukdlHNLURj2nwi.XsegUTYEWy2z12xwG', '1', '1', NULL, '2024-02-08 10:29:02', NULL),
(4, 'admin4', 'admin4', 'admin4', NULL, '$2y$10$fKCLFpNqwmIrpoiAgcL0Tu5gZ0T//e9g5za6/NjknoaNN8AJAE2te', '1', '1', NULL, '2024-02-08 10:29:19', NULL),
(5, 'admin5', 'admin5', 'admin5', NULL, '$2y$10$qRajGptg9vAJizPlhaf9ZOU1T092rQgeqbZt.am8/K68BD88D64Bq', '1', '1', NULL, '2024-02-08 10:29:33', NULL),
(6, 'user1', 'user1', 'v', NULL, '$2y$10$lmxzQ12q0NitqY3utGcWiuTInIdFnrWl8bFIxL23PFBftD45VdjTW', '1', '2', NULL, '2024-02-08 10:29:48', NULL),
(7, 'user2', 'user2', 'user2', NULL, '$2y$10$O9aXQLRxXqi.NKekhHJc1eW8dDm7KwRcWSDd/KR6MThOcbeO629l6', '1', '2', NULL, '2024-02-08 10:30:03', NULL),
(8, 'user3', 'user3', 'user3', NULL, '$2y$10$N8viPja2ZlRxgtw4MDKMNez3KLmIdl2qMcF8dB8Jq4U2uHzMijOsa', '1', '2', NULL, '2024-02-08 10:30:19', NULL),
(9, 'user4', 'user4', 'user4', NULL, '$2y$10$zi5GwsFf2uBilFJEuQugO.zKJZq.cRdWEkfre9/3aKsta/O9HP2F6', '1', '2', NULL, '2024-02-08 10:30:37', NULL),
(10, 'user5', 'user5', 'user5', NULL, '$2y$10$n7K3she.9qDfENUALPHg8u3pQrErIw2w7TnCoaxwqoNjuyBY6DL.G', '1', '2', NULL, '2024-02-08 10:30:54', NULL),
(11, 'admin', 'admin', 'admin', NULL, '$2y$10$EfxwDcOh8jCxRXKlTGOrCurxXfmMmc6CHSOxhUF5oahxlRDdRgKVG', '1', '1', NULL, '2024-02-08 10:31:17', NULL),
(12, 'user', 'user', 'user', NULL, '$2y$10$kjGw7kivqI87Ar6mVMZF/.A/yn.BphEan4nYGdkzV3khVgx7mzTqa', '1', '2', NULL, '2024-02-08 10:48:47', NULL);

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `barang_details`
--
ALTER TABLE `barang_details`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `barang_headers`
--
ALTER TABLE `barang_headers`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `barang_headers_no_permintaan_unique` (`no_permintaan`);

--
-- Indeks untuk tabel `master_barangs`
--
ALTER TABLE `master_barangs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `master_barangs_kode_barang_unique` (`kode_barang`);

--
-- Indeks untuk tabel `master_karyawans`
--
ALTER TABLE `master_karyawans`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `master_karyawans_no_ktp_unique` (`no_ktp`);

--
-- Indeks untuk tabel `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_user_unique` (`user`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `barang_details`
--
ALTER TABLE `barang_details`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT untuk tabel `barang_headers`
--
ALTER TABLE `barang_headers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `master_barangs`
--
ALTER TABLE `master_barangs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT untuk tabel `master_karyawans`
--
ALTER TABLE `master_karyawans`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT untuk tabel `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
