@extends('layouts.app')
@section('content')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.2.0/sweetalert2.min.css">
    <div class="main-content-inner">
        <!-- sales report area start -->
        <div class="sales-report-area mt-5 mb-5">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <button type="button" class="btn btn-info mb-3" id="select_print">Export Excel <span class="glyphicon glyphicon-file"></span></button>
                        &nbsp; &nbsp; &nbsp;
                        <button type="button" class="btn btn-primary mb-3" data-toggle="modal" data-target="#btn-add"><i class="ti-plus"></i> &nbsp; Tambah Master Barang
                        </button>
                        @if(count($errors) > 0 )
                            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <ul class="p-0 m-0" style="list-style: none;">
                                    @foreach($errors->all() as $error)
                                    <li>{{$error}}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <div class="data-tables" style="margin-top: 20px;">
                            <table id="detailBarang" class="text-center" width="100%">
                                <thead class="bg-light text-capitalize">
                                    <tr>
                                        <th>No</th>
                                        <th>Kode Barang</th>
                                        <th>Nama Barang</th>
                                        <th>Lokasi Barang</th>
                                        <th>Stok</th>
                                        <th>Satuan</th>
                                        <th>Harga Barang</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- Modal Tambah-->
                    <div class="modal fade" id="btn-add">
                        <div class="modal-dialog modal-dialog-centered" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                <i class="ti-plus"></i>
                                    &nbsp; &nbsp; <h5 class="modal-title">Tambah Master barang</h5>
                                    <button type="button" class="close" data-dismiss="modal"><span>&times;</span></button>
                                </div>
                                @if(count($errors) > 0 )
                                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                        <ul class="p-0 m-0" style="list-style: none;">
                                            @foreach($errors->all() as $error)
                                            <li>{{$error}}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif
                                <div class="modal-body">
                                    <form action="{{ url('/MasterBarang') }}" id="formDetailAdd" nama="formDetailAdd" method="POST">
                                    @csrf
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div class="form-gp">
                                                        <label for="kode_barang">Kode Barang <span class="symbol required"></span></label>
                                                        <input type="text" readonly id="kode_barang" name="kode_barang" value="{{$MasterBarang}}" aria-required="true">
                                                        
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="nama_barang">Nama Barang<sup style="color: red">*</sup></label>
                                                        <div class="input-group mb-3">
                                                            <input type="nama_barang" id="nama_barang" class="form-control " name="nama_barang" placeholder="Masukan Nama Barang" required>
                                                        </div>
                                                        @if($errors->has('nama_barang'))
                                                            <span class="alert-message">Nama Barang Harus Diisi</span>
                                                            @endif
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="lokasi_barang">Lokasi Barang<sup style="color: red">*</sup></label>
                                                        <input class="form-control @error('lokasi_barang') is-invalid @enderror" id="lokasi_barang" name="lokasi_barang" type="text" placeholder="Masukan Lokasi Barang" value="{{old('lokasi_barang')}}"required>
                                                        @if($errors->has('lokasi_barang'))
                                                            <span class="alert-message">{{$errors->first('lokasi_barang')}}</span>
                                                        @endif
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="stok">Stok<sup style="color: red">*</sup></label>
                                                        <input class="form-control @error('stok') is-invalid @enderror" onkeypress="return isNumber(event)" id="stok" name="stok" type="text" placeholder="Masukan Stok" value="{{old('stok')}}"required>
                                                        @if($errors->has('stok'))
                                                            <span class="alert-message">{{$errors->first('stok')}}</span>
                                                        @endif
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="satuan">Satuan<sup style="color: red">*</sup></label>
                                                        <select class="form-control form-control-lg" name="satuan" id="satuan" required>
                                                        <option value="">Pilih</option>
                                                            <option value="PCS">PCS</option>
                                                            <option value="LEMBAR">LEMBAR</option>
                                                            <option value="LUSIN">LUSIN</option>
                                                            <option value="PAK">PAK</option>
                                                        </select>
                                                        @if($errors->has('satuan'))
                                                            <span class="alert-message">{{$errors->first('satuan')}}</span>
                                                        @endif
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="Harga Barang">Harga Barang<sup style="color: red">*</sup></label>
                                                        <input class="form-control price" id="harga_barang" name="harga_barang" onfocus='sum1()' onblur='sum()' value="0"required>
                                                        @if($errors->has('Harga Barang'))
                                                            <span class="alert-message">{{$errors->first('Harga Barang')}}</span>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                                            <button type="submit" class="btn btn-primary">Simpan</button>
                                        </div>
                                    </form>    
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Modal Edit-->
                    <div class="modal fade" id="btn-edit">
                        <div class="modal-dialog modal-dialog-centered" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                <i class="ti-plus"></i>
                                    &nbsp; &nbsp; <h5 class="modal-title">Edit Master Barang</h5>
                                    <button type="button" class="close" data-dismiss="modal"><span>&times;</span></button>
                                </div>
                                @if(count($errors) > 0 )
                                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                        <ul class="p-0 m-0" style="list-style: none;">
                                            @foreach($errors->all() as $error)
                                            <li>{{$error}}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif
                                <div class="modal-body">
                                    <form action="{{ url('/MasterBarang') }}"  id="formDetailEdit" nama="formDetailEdit"  method="POST">
                                        @csrf
                                        <!-- @method('PUT') -->
                                        <div class="card-body">
                                            <div class="row">
                                            <input type="hidden" required id="id_master_barang" name="id" aria-required="true">
                                                <div class="col-sm-12">
                                                    <div class="form-gp">
                                                        <label for="kode_barang">Kode Barang <span class="symbol required"></span></label>
                                                        <input type="text" readonly id="edit_kode_barang" name="kode_barang" value="{{$MasterBarang}}" aria-required="true">
                                                        
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="nama_barang">Nama Barang<sup style="color: red">*</sup></label>
                                                        <div class="input-group mb-3">
                                                            <input type="nama_barang" id="edit_nama_barang" class="form-control " name="nama_barang" placeholder="Masukan Nama Barang" required>
                                                        </div>
                                                        @if($errors->has('nama_barang'))
                                                            <span class="alert-message">Nama Barang Harus Diisi</span>
                                                            @endif
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="lokasi_barang">Lokasi Barang<sup style="color: red">*</sup></label>
                                                        <input class="form-control @error('lokasi_barang') is-invalid @enderror" id="edit_lokasi_barang" name="lokasi_barang" type="text" placeholder="Masukan Lokasi Barang" value="{{old('lokasi_barang')}}"required>
                                                        @if($errors->has('lokasi_barang'))
                                                            <span class="alert-message">{{$errors->first('lokasi_barang')}}</span>
                                                        @endif
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="stok">Stok<sup style="color: red">*</sup></label>
                                                        <input class="form-control @error('stok') is-invalid @enderror" id="edit_stok" onkeypress="return isNumber(event)" name="stok" type="text" placeholder="Masukan Stok" value="{{old('stok')}}"required>
                                                        @if($errors->has('stok'))
                                                            <span class="alert-message">{{$errors->first('stok')}}</span>
                                                        @endif
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="satuan">Satuan<sup style="color: red">*</sup></label>
                                                        <select class="form-control form-control-lg" id="edit_satuan" name="satuan" required>
                                                        <option value="">Pilih</option>
                                                            <option value="PCS">PCS</option>
                                                            <option value="LEMBAR">LEMBAR</option>
                                                            <option value="LUSIN">LUSIN</option>
                                                            <option value="PAK">PAK</option>
                                                        </select>
                                                        @if($errors->has('satuan'))
                                                            <span class="alert-message">{{$errors->first('satuan')}}</span>
                                                        @endif
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="Harga Barang">Harga Barang<sup style="color: red">*</sup></label>
                                                        <input class="form-control price" id="edit_harga_barang" name="harga_barang" onfocus='sum1edit()' onblur='sumedit()' value="0"required>
                                                        @if($errors->has('Harga Barang'))
                                                            <span class="alert-message">{{$errors->first('Harga Barang')}}</span>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                                            <button type="submit" class="btn btn-primary">Simpan Perubahan</button>
                                        </div>
                                    </form>    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- overview area end -->
    </div>
@endsection
@section('javascript')
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.2.0/sweetalert2.all.min.js"></script>
<script src="https://cdn.datatables.net/buttons/2.2.3/js/dataTables.buttons.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdn.datatables.net/buttons/2.2.3/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/2.2.3/js/buttons.print.min.js"></script>

@if( $massage = session('success'))
    <script type="text/javascript">
    Swal.fire({
        type: 'success',
        title:  "{{ $massage }}",
        text: 'success',
        timer: 4000,
    })
    </script>   
@endif
@if ( $massage = session('gagal'))
    <script type="text/javascript">
    Swal.fire({
        type: 'error',
        title:  "{{ $massage }}",
        text: 'Gagal',
        timer: 4000,
    })
@endif 
<script type="text/javascript">
    function isNumber(evt) {
		evt = (evt) ? evt : window.event;
		var charCode = (evt.which) ? evt.which : evt.keyCode; 
		if (charCode == 46 ) {
			return true; 
		}
		if (charCode > 31 && (charCode < 48 || charCode > 57)) {
			return false; 
		}
		return true;
	}
    $(document).ready(function () {
        $("#satuan").select2({
            width: '100%',
            allowClear: true,
            dropdownCssClass: "bigdrop",
            minimumInputLength: 0,
				language: {
					inputTooShort: function() {
						return '--Pilih--';
					}
				},
        })
        $("#edit_satuan").select2({
            width: '100%',
            allowClear: true,
            dropdownCssClass: "bigdrop",
            minimumInputLength: 0,
				language: {
					inputTooShort: function() {
						return '--Pilih--';
					}
				},
        })
        var table = $('#detailBarang').DataTable({
            destroy: true,
            responsive: true,
            processing: true,
            serverSide: true,
            language: {
                processing: '<span>Harap Menunggu...<i class="fa fa-spinner fa-spin fa-fw"></i></span>',
                lengthMenu: "_MENU_ data per halaman",
                sSearch: "Cari:",
                info : "Total data : _MAX_",
            },
            ajax: "{{ route('MasterBarang.index') }}",
            columns: [
                { "data": null,"sortable": false, 
                    render: function (data, type, row, meta) {
                    return meta.row + meta.settings._iDisplayStart + 1;
                    }  
                },
                {data: 'kode_barang', name: 'kode_barang'},
                {data: 'nama_barang', name: 'nama_barang'},
                {data: 'lokasi_barang', name: 'lokasi_barang'},
                {data: 'stok', name: 'stok'},
                {data: 'satuan', name: 'satuan'},
                {data: 'harga_barang', name: 'harga_barang',render: $.fn.dataTable.render.number( ',', '.', 2 )},
                {data: 'action', name: 'action', orderable: false, searchable: false},
            ],
            buttons: [{
                        extend:'excelHtml5', 
                        text: 'Laporan Master Barang',
                        exportOptions: {
                            columns: [0,1,2,3,4,5,6],
                            modifier: {
                                search: 'applied',
                                order: 'applied'
                            }
                        },
                        title: 'Master Barang',
                        customize: function ( xlsx ) {
                            var sheet = xlsx.xl.worksheets['sheet1.xml'];
                            $('row c[r^="A"]', sheet).attr( 's', '50', ); //<-- left aligned text
                            $('row c[r^="B"]', sheet).attr( 's', '55' ); //<-- wrapped text
                            // align right
                            $('row c[r^="C"]', sheet).attr( 's', '52' );
                            $('row c[r^="D"]', sheet).attr( 's', '52' );
                            $('row c[r^="E"]', sheet).attr( 's', '52' );
                            $('row c[r^="F"]', sheet).attr( 's', '52' );
                            $('row c[r^="G"]', sheet).attr( 's', '52' );
                            $('row c[r^="H"]', sheet).attr( 's', '52' );
                            $('row c[r^="I"]', sheet).attr( 's', '52' );
                            $('row c[r^="J"]', sheet).attr( 's', '52' );
                            $('row c[r^="K"]', sheet).attr( 's', '55' ); //<-- wrapped text
                        } 
                    }]
        });
        $("#select_print").on("click", function() {
                table.button( '.buttons-excel' ).trigger();
        })
        $('#formDetailAdd').on('submit', (function (e) {
			e.preventDefault();
		
			// $('#btn_simpan_eorderlabdet_add').attr('disabled', 'disabled');
				
			$.ajax({
				url: $(this).attr('action'),
				type: "POST",
				data: new FormData(this),
				dataType: 'json',
				contentType: false,
				cache: false,
				processData: false,
				success: function (data) {
					// $('#btn-simpan').removeAttr('disabled');
					
					if (data.status == "error") {
						Swal.fire({
                            type: 'error',
                            title:  "Tidak Bisa Disimpan",
                            text: data.message,
                            timer: 4000,
                        })
						
					} else {
						$('#formDetailAdd').trigger("reset");
                        $('#btn-add').modal('hide');
						Swal.fire({
                            type: 'success',
                            title:  "success!",
                            text: data.message,
                            timer: 4000,
                        })
                        $('#detailBarang').DataTable().ajax.reload(null, false);
                        window.location.reload();
					}

				},
				error: function () {
                    Swal.fire({
                        type: 'error',
                        title:  "Tidak Bisa Disimpan",
                        text: 'Tidak Bisa Disimpan"',
                        timer: 4000,
                    })
				}
			});

		}));
        $('#formDetailEdit').on('submit', (function (e) {
			e.preventDefault();
		
			// $('#btn_simpan_eorderlabdet_add').attr('disabled', 'disabled');
				
			$.ajax({
				url: $(this).attr('action'),
				type: "POST",
				data: new FormData(this),
				dataType: 'json',
				contentType: false,
				cache: false,
				processData: false,
				success: function (data) {
					// $('#btn-simpan').removeAttr('disabled');
					
					if (data.status == "error") {
						Swal.fire({
                            type: 'error',
                            title:  "Tidak Bisa Disimpan",
                            text: data.message,
                            timer: 4000,
                        })
						
					} else {
						$('#formDetailEdit').trigger("reset");
                        $('#btn-edit').modal('hide');
						Swal.fire({
                            type: 'success',
                            title:  "success!",
                            text: data.message,
                            timer: 4000,
                        })
                        $('#detailBarang').DataTable().ajax.reload(null, false);
					}

				},
				error: function () {
                    Swal.fire({
                        type: 'error',
                        title:  "Tidak Bisa Disimpan",
                        text: 'Tidak Bisa Disimpan"',
                        timer: 4000,
                    })
				}
			});

		}));
    })
    function sum() {
        var nom  = document.getElementById('harga_barang').value;
            if(nom==""){
                nom=0
            }
        document.getElementById('harga_barang').value = nom.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
        //nom.toLocaleString('en-US', {maimumFractionDigits:2});    
    }
    function sum1() {
        var nom  = document.getElementById('harga_barang').value;
        if(nom==""){
                nom=0 
            }
        document.getElementById('harga_barang').value = nom.toString().replaceAll(",", "");
    //nom.toLocaleString('en-US', {maximumFractionDigits:2});      
    }
    function sumedit() {
        var nom  = document.getElementById('edit_harga_barang').value;
            if(nom==""){
                nom=0
            }
        document.getElementById('edit_harga_barang').value = nom.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
        //nom.toLocaleString('en-US', {maimumFractionDigits:2});    
    }
    function sum1edit() {
        var nom  = document.getElementById('edit_harga_barang').value;
        if(nom==""){
                nom=0 
            }
        document.getElementById('edit_harga_barang').value = nom.toString().replaceAll(",", "");
    //nom.toLocaleString('en-US', {maximumFractionDigits:2});      
    }
  
    $('body').on('click', '.editItem', function () {
        let id_user = $(this).data('id');
        console.log(id_user)
        document.getElementById('id_master_barang').value = ''
        document.getElementById('edit_kode_barang').value = ''
        document.getElementById('edit_nama_barang').value = ''
        document.getElementById('edit_lokasi_barang').value = ''
        document.getElementById('edit_stok').value = ''
        document.getElementById('edit_harga_barang').value = ''
        $("#edit_satuan").val('').trigger('change');

        $.ajax({
            url: '/MasterBarang/' + id_user,
            success: function (res) {
                console.log(res);
                    document.getElementById('id_master_barang').value = res.id
                    document.getElementById('edit_nama_barang').value = res.nama_barang
                    document.getElementById('edit_kode_barang').value = res.kode_barang
                    document.getElementById('edit_lokasi_barang').value = res.lokasi_barang
                    document.getElementById('edit_stok').value = res.stok
                    document.getElementById('edit_harga_barang').value = res.harga_barang
                    $("#edit_satuan").val(res.satuan).trigger('change');
                    $('#btn-edit').modal('show')
            }
        })
    });
    
    $('body').on('click', '.deleteItem', function () {
        var user_id = $(this).data("id");
        
        swal({
            title: "Delete?",
            text: "Yakin Mau Delete ?",
            type: "warning",
            showCancelButton: !0,
            confirmButtonText: "Yakin",
            cancelButtonText: "Tidak!",
            reverseButtons: !0
        }).then(function (e) {
            if (e.value === true) {
            $.ajax({
                type: "DELETE",
                url: "{{ route('MasterBarang.store') }}"+'/'+user_id,
                data: {
                    "_token": "{{ csrf_token() }}",
                    "user_id": user_id
                    },
                    success: function (data) {
                    $('#ItemForm').trigger("reset");
                    $('#ajaxModel').modal('hide');
                    Swal.fire({
                        type: 'success',
                        title:  "Data Sukses Dihapus",
                        text: 'Data Sukses Dihapus"',
                        timer: 4000,
                    })
                    $('#detailBarang').DataTable().ajax.reload(null, false);
                },
                error: function (data) {
                    Swal.fire({
                        type: 'error',
                        title:  "Tidak Bisa Dihapus",
                        text: 'Tidak Bisa Dihapus"',
                        timer: 4000,
                    })
                }
                });
            } else {
                e.dismiss;
            }  
        }, function (dismiss) {
            return false;
        })
    });
</script>
@endsection