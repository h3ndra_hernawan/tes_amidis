@extends('layouts.app')
@section('content')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.2.0/sweetalert2.min.css">
    <div class="main-content-inner">
        <!-- sales report area start -->
        <div class="sales-report-area mt-5 mb-5">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <button type="button" class="btn btn-info mb-3" id="select_print">Export Excel <span class="glyphicon glyphicon-file"></span></button>
                        &nbsp; &nbsp; &nbsp;
                        <button type="button" class="btn btn-primary mb-3" data-toggle="modal" data-target="#btn-add"><i class="ti-plus"></i> &nbsp; Tambah Master Kayawan
                        </button>
                        @if(count($errors) > 0 )
                            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <ul class="p-0 m-0" style="list-style: none;">
                                    @foreach($errors->all() as $error)
                                    <li>{{$error}}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <div class="data-tables" style="margin-top: 20px;">
                            <table id="detailBarang" class="text-center" width="100%">
                                <thead class="bg-light text-capitalize">
                                    <tr>
                                        <th>No</th>
                                        <th>No Ktp</th>
                                        <th>Nama</th>
                                        <th>Departemen</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- Modal Tambah-->
                    <div class="modal fade" id="btn-add">
                        <div class="modal-dialog modal-dialog-centered" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                <i class="ti-plus"></i>
                                    &nbsp; &nbsp; <h5 class="modal-title">Tambah Master Kayawan</h5>
                                    <button type="button" class="close" data-dismiss="modal"><span>&times;</span></button>
                                </div>
                                @if(count($errors) > 0 )
                                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                        <ul class="p-0 m-0" style="list-style: none;">
                                            @foreach($errors->all() as $error)
                                            <li>{{$error}}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif
                                <div class="modal-body">
                                    <form action="{{ url('/MasterKaryawan') }}" id="formDetailAdd" nama="formDetailAdd" method="POST">
                                    @csrf
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div class="form-gp">
                                                        <label for="no_ktp">No Ktp<span class="symbol required"></span></label>
                                                        <input type="text" onkeypress="return isNumber(event)" id="no_ktp" name="no_ktp" aria-required="true">
                                                        
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="nama">Nama<sup style="color: red">*</sup></label>
                                                        <div class="input-group mb-3">
                                                            <input type="nama" id="nama" class="form-control " name="nama" placeholder="Masukan Nama" required>
                                                        </div>
                                                        @if($errors->has('nama'))
                                                            <span class="alert-message">Nama Harus Diisi</span>
                                                            @endif
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="departemen">Departemen<sup style="color: red">*</sup></label>
                                                        <input class="form-control @error('departemen') is-invalid @enderror" id="departemen" name="departemen" type="text" placeholder="Masukan Departemen" value="{{old('departemen')}}"required>
                                                        @if($errors->has('departemen'))
                                                            <span class="alert-message">{{$errors->first('departemen')}}</span>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                                            <button type="submit" class="btn btn-primary">Simpan</button>
                                        </div>
                                    </form>    
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Modal Edit-->
                    <div class="modal fade" id="btn-edit">
                        <div class="modal-dialog modal-dialog-centered" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                <i class="ti-plus"></i>
                                    &nbsp; &nbsp; <h5 class="modal-title">Edit Master Karyawan</h5>
                                    <button type="button" class="close" data-dismiss="modal"><span>&times;</span></button>
                                </div>
                                @if(count($errors) > 0 )
                                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                        <ul class="p-0 m-0" style="list-style: none;">
                                            @foreach($errors->all() as $error)
                                            <li>{{$error}}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif
                                <div class="modal-body">
                                    <form action="{{ url('/MasterKaryawan') }}"  id="formDetailEdit" nama="formDetailEdit"  method="POST">
                                        @csrf
                                        <!-- @method('PUT') -->
                                        <div class="card-body">
                                            <div class="row">
                                            <input type="hidden" required id="id_karyawan_barang" name="id" aria-required="true">
                                                <div class="col-sm-12">
                                                    <div class="form-gp">
                                                        <label for="no_ktp">No Ktp<span class="symbol required"></span></label>
                                                        <input type="text" onkeypress="return isNumber(event)"  id="edit_no_ktp" name="no_ktp" aria-required="true">
                                                        
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="nama">Nama<sup style="color: red">*</sup></label>
                                                        <div class="input-group mb-3">
                                                            <input type="nama" id="edit_nama" class="form-control " name="nama" placeholder="Masukan Nama" required>
                                                        </div>
                                                        @if($errors->has('nama'))
                                                            <span class="alert-message">Nama Harus Diisi</span>
                                                            @endif
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="departemen">Departemen<sup style="color: red">*</sup></label>
                                                        <input class="form-control @error('departemen') is-invalid @enderror" id="edit_departemen" name="departemen" type="text" placeholder="Masukan Departemen" value="{{old('departemen')}}"required>
                                                        @if($errors->has('departemen'))
                                                            <span class="alert-message">{{$errors->first('departemen')}}</span>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                                            <button type="submit" class="btn btn-primary">Simpan Perubahan</button>
                                        </div>
                                    </form>    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- overview area end -->
    </div>
@endsection
@section('javascript')
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.2.0/sweetalert2.all.min.js"></script>
<script src="https://cdn.datatables.net/buttons/2.2.3/js/dataTables.buttons.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdn.datatables.net/buttons/2.2.3/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/2.2.3/js/buttons.print.min.js"></script>

@if( $massage = session('success'))
    <script type="text/javascript">
    Swal.fire({
        type: 'success',
        title:  "{{ $massage }}",
        text: 'success',
        timer: 4000,
    })
    </script>   
@endif
@if ( $massage = session('gagal'))
    <script type="text/javascript">
    Swal.fire({
        type: 'error',
        title:  "{{ $massage }}",
        text: 'Gagal',
        timer: 4000,
    })
@endif 
<script type="text/javascript">
    function isNumber(evt) {
		evt = (evt) ? evt : window.event;
		var charCode = (evt.which) ? evt.which : evt.keyCode; 
		if (charCode == 46 ) {
			return true; 
		}
		if (charCode > 31 && (charCode < 48 || charCode > 57)) {
			return false; 
		}
		return true;
	}
    $(document).ready(function () {
        var no_ktp = document.getElementById('no_ktp');
        no_ktp.addEventListener("keyup", function(e) {
        // tambahkan 'Rp.' pada saat form di ketik
        // gunakan fungsi formatRupiah() untuk mengubah angka yang di ketik menjadi format angka
            if(no_ktp.value.length >16){
                Swal.fire({
                    type: 'warning',
                    title:  "Maksimal 16 Karekter",
                    timer: 4000,
                })
                no_ktp.value = no_ktp.value.slice(0, 16);
            }
        });
        var edit_no_ktp = document.getElementById('edit_no_ktp');
        edit_no_ktp.addEventListener("keyup", function(e) {
        // tambahkan 'Rp.' pada saat form di ketik
        // gunakan fungsi formatRupiah() untuk mengubah angka yang di ketik menjadi format angka
            if(edit_no_ktp.value.length >16){
                Swal.fire({
                    type: 'warning',
                    title:  "Maksimal 16 Karekter",
                    timer: 4000,
                })
                edit_no_ktp.value = edit_no_ktp.value.slice(0, 16);
            }
        });
        var table = $('#detailBarang').DataTable({
            destroy: true,
            responsive: true,
            processing: true,
            serverSide: true,
            language: {
                processing: '<span>Harap Menunggu...<i class="fa fa-spinner fa-spin fa-fw"></i></span>',
                lengthMenu: "_MENU_ data per halaman",
                sSearch: "Cari:",
                info : "Total data : _MAX_",
            },
            ajax: "{{ route('MasterKaryawan.index') }}",
            columns: [
                { "data": null,"sortable": false, 
                    render: function (data, type, row, meta) {
                    return meta.row + meta.settings._iDisplayStart + 1;
                    }  
                },
                {data: 'no_ktp', name: 'no_ktp'},
                {data: 'nama', name: 'nama'},
                {data: 'departemen', name: 'departemen'},
                {data: 'action', name: 'action', orderable: false, searchable: false},
            ],
            buttons: [{
                        extend:'excelHtml5', 
                        text: 'Laporan Master Barang',
                        exportOptions: {
                            columns: [0,1,2,3,4,5],
                            modifier: {
                                search: 'applied',
                                order: 'applied'
                            }
                        },
                        title: 'Master Barang',
                        customize: function ( xlsx ) {
                            var sheet = xlsx.xl.worksheets['sheet1.xml'];
                            $('row c[r^="A"]', sheet).attr( 's', '50', ); //<-- left aligned text
                            $('row c[r^="B"]', sheet).attr( 's', '55' ); //<-- wrapped text
                            // align right
                            $('row c[r^="C"]', sheet).attr( 's', '52' );
                            $('row c[r^="D"]', sheet).attr( 's', '52' );
                            $('row c[r^="E"]', sheet).attr( 's', '52' );
                            $('row c[r^="F"]', sheet).attr( 's', '52' );
                            $('row c[r^="G"]', sheet).attr( 's', '52' );
                            $('row c[r^="H"]', sheet).attr( 's', '52' );
                            $('row c[r^="I"]', sheet).attr( 's', '52' );
                            $('row c[r^="J"]', sheet).attr( 's', '52' );
                            $('row c[r^="K"]', sheet).attr( 's', '55' ); //<-- wrapped text
                        } 
                    }]
        });
        $("#select_print").on("click", function() {
                table.button( '.buttons-excel' ).trigger();
        })
        $('#formDetailAdd').on('submit', (function (e) {
			e.preventDefault();
		
			// $('#btn_simpan_eorderlabdet_add').attr('disabled', 'disabled');
				
			$.ajax({
				url: $(this).attr('action'),
				type: "POST",
				data: new FormData(this),
				dataType: 'json',
				contentType: false,
				cache: false,
				processData: false,
				success: function (data) {
					// $('#btn-simpan').removeAttr('disabled');
					
					if (data.status == "error") {
						Swal.fire({
                            type: 'error',
                            title:  "Tidak Bisa Disimpan",
                            text: data.message,
                            timer: 4000,
                        })
						
					} else {
						$('#formDetailAdd').trigger("reset");
                        $('#btn-add').modal('hide');
						Swal.fire({
                            type: 'success',
                            title:  "success!",
                            text: data.message,
                            timer: 4000,
                        })
                        $('#detailBarang').DataTable().ajax.reload(null, false);
					}

				},
				error: function () {
                    Swal.fire({
                        type: 'error',
                        title:  "Tidak Bisa Disimpan",
                        text: 'Tidak Bisa Disimpan"',
                        timer: 4000,
                    })
				}
			});

		}));
        $('#formDetailEdit').on('submit', (function (e) {
			e.preventDefault();
		
			// $('#btn_simpan_eorderlabdet_add').attr('disabled', 'disabled');
				
			$.ajax({
				url: $(this).attr('action'),
				type: "POST",
				data: new FormData(this),
				dataType: 'json',
				contentType: false,
				cache: false,
				processData: false,
				success: function (data) {
					// $('#btn-simpan').removeAttr('disabled');
					
					if (data.status == "error") {
						Swal.fire({
                            type: 'error',
                            title:  "Tidak Bisa Disimpan",
                            text: data.message,
                            timer: 4000,
                        })
						
					} else {
						$('#formDetailEdit').trigger("reset");
                        $('#btn-edit').modal('hide');
						Swal.fire({
                            type: 'success',
                            title:  "success!",
                            text: data.message,
                            timer: 4000,
                        })
                        $('#detailBarang').DataTable().ajax.reload(null, false);
					}

				},
				error: function () {
                    Swal.fire({
                        type: 'error',
                        title:  "Tidak Bisa Disimpan",
                        text: 'Tidak Bisa Disimpan"',
                        timer: 4000,
                    })
				}
			});

		}));
    })
  
    $('body').on('click', '.editItem', function () {
        let id_user = $(this).data('id');
        console.log(id_user)
        document.getElementById('id_karyawan_barang').value = ''
        document.getElementById('edit_no_ktp').value = ''
        document.getElementById('edit_nama').value = ''
        document.getElementById('edit_departemen').value = ''
        $("#edit_satuan").val('').trigger('change');

        $.ajax({
            url: '/MasterKaryawan/' + id_user,
            success: function (res) {
                console.log(res);
                    document.getElementById('id_karyawan_barang').value = res.id
                    document.getElementById('edit_nama').value = res.nama
                    document.getElementById('edit_no_ktp').value = res.no_ktp
                    document.getElementById('edit_departemen').value = res.departemen
                    $('#btn-edit').modal('show')
            }
        })
    });
    
    $('body').on('click', '.deleteItem', function () {
        var user_id = $(this).data("id");
        
        swal({
            title: "Delete?",
            text: "Yakin Mau Delete ?",
            type: "warning",
            showCancelButton: !0,
            confirmButtonText: "Yakin",
            cancelButtonText: "Tidak!",
            reverseButtons: !0
        }).then(function (e) {
            if (e.value === true) {
            $.ajax({
                type: "DELETE",
                url: "{{ route('MasterKaryawan.store') }}"+'/'+user_id,
                data: {
                    "_token": "{{ csrf_token() }}",
                    "user_id": user_id
                    },
                    success: function (data) {
                    $('#ItemForm').trigger("reset");
                    $('#ajaxModel').modal('hide');
                    Swal.fire({
                        type: 'success',
                        title:  "Data Sukses Dihapus",
                        text: 'Data Sukses Dihapus"',
                        timer: 4000,
                    })
                    $('#detailBarang').DataTable().ajax.reload(null, false);
                },
                error: function (data) {
                    Swal.fire({
                        type: 'error',
                        title:  "Tidak Bisa Dihapus",
                        text: 'Tidak Bisa Dihapus"',
                        timer: 4000,
                    })
                }
                });
            } else {
                e.dismiss;
            }  
        }, function (dismiss) {
            return false;
        })
    });
</script>
@endsection