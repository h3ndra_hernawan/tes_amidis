@extends('layouts.app')
@section('content')
    <div class="main-content-inner">
        <!-- sales report area start -->
        <div class="sales-report-area mt-5 mb-5">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <h1><i class="ti-user"> </i>@php echo session()->get('nama') @endphp</h1>
                    </div> 
                </div>
            </div> 
        </div>
        <div class="sales-report-area mt-5 mb-5">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        @if (session('success'))
                        <div class="alert alert-success border-0 alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert"><span>&times;</span></button>
                            {{ session('success') }}.
                        </div>
                        @endif

                        @if (session('gagal'))
                        <div class="alert alert-danger border-0 alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert"><span>&times;</span></button>
                            {{ session('gagal') }}.
                        </div>
                        @endif
                        @if(count($errors) > 0 )
                            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <ul class="p-0 m-0" style="list-style: none;">
                                    @foreach($errors->all() as $error)
                                    <li>{{$error}}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
            
                        <div class="alert alert-danger border-0 alert-dismissible" style="display: none" id="tes1">
                            <button type="button" class="close" data-dismiss="alert"><span>&times;</span></button>
                            <p id="text-tes1"></p>
                        </div>
                        <div class="card">
                        <div class="card-header">
                            <button type="button" class="btn btn-primary" id="btn-add">
                                <i class="icon-plus"></i> Tambah Permintaan Barang
                            </button>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="table-responsive">
                                    <div class="col-md-12">
                                        <table class="table table-bordered table-hover datatable-basic" width="100%" id="dt-table" >
                                            <thead class="bg-light text-capitalize">
                                                <tr>
                                                    <th style="width: 5%">No</th>
                                                    <th>No Permintaan Barang</th>
                                                    <th>Nik</th>
                                                    <th>Nama</th>
                                                    <th>Departemen </th>
                                                    <th>Tanggal Permintaan</th>
                                                    <th>Total</th>
                                                    <th style="width: 20%"></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <!-- Modal -->
                            <div class="modal fade cd-example-modal-xl" id="modal_add" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog modal-xl modal-primary">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel"><i class="icon-user-follow"></i> Tambah Permintaan Barang</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        @if(count($errors) > 0 )
                                            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                                <ul class="p-0 m-0" style="list-style: none;">
                                                    @foreach($errors->all() as $error)
                                                    <li>{{$error}}</li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        @endif
                                        <div class="modal-body">
                                            <form action="{{ url('/PermintaanBarang') }}" id="formDetailAdd" name="formDetailAdd" method="POST"  name="autoSumForm" enctype="multipart/form-data">
                                                @csrf
                                                <div class="card-body">
                                                        <div class="form-row">
                                                            <div class="orm-group col-md-4">
                                                                <label for="no_ktp">NIK Peminta<sup style="color: red">*</sup></label>
                                                                <select class="select2 form-control" data-fouc id="select-nik" onblur=validate() onchange=validate() required>
                                                                    <option value="">-- Pilih Nik --</option>
                                                                    @foreach ($MasterKaryawan as $item)
                                                                        <option value="{{ $item['no_ktp'] }},{{ $item['nama'] }},{{ $item['departemen'] }}" >{{ $item['no_ktp'] }}</option>
                                                                    @endforeach
                                                                </select>
                                                                <input type="hidden" id="no_ktp" name="no_ktp">
                                                                <span class="form-text text-danger" id="span-level" style="display: none">Pilih No Ktp</span>
                                                                @if($errors->has('no_ktp'))
                                                                    <span class="alert-message">{{$errors->first('no_ktp')}}</span>
                                                                @endif
                                                            </div>
                                                            <div class="orm-group col-md-4"> 
                                                                <label for="nama">Nama<sup style="color: red">*</sup></label>
                                                                    <input class="form-control @error('nama') is-invalid @enderror" readonly id="nama" name="nama" type="text"  placeholder="Masukan Nama" required>
                                                                @if($errors->has('nama'))
                                                                    <span class="alert-message">{{$errors->first('nama')}}</span>
                                                                @endif
                                                            </div>    
                                                            
                                                            <div class="orm-group col-md-4"> 
                                                                <label for="departemen">Departemen<sup style="color: red">*</sup></label>  
                                                                    <input class="form-control @error('departemen') is-invalid @enderror" readonly id="departemen" name="departemen" type="text" placeholder="Masukan Departemen" required>
                                                                @if($errors->has('departemen'))
                                                                    <span class="alert-message">{{$errors->first('departemen')}}</span>
                                                                @endif
                                                            </div>  
                                                        </div>
                                                        <div class="form-row">
                                                            <div class="form-group col-md-4">
                                                                <label for="no_permintaan">No Permintaan<sup style="color: red">*</sup></label>
                                                                <input class="form-control @error('no_permintaan') is-invalid @enderror" readonly id="no_permintaan" name="no_permintaan" type="text" onblur=validate() onchange=validate() value="{{ $new_date}}"placeholder="Masukan No Permintaa"required>
                                                                @if($errors->has('no_permintaan'))
                                                                    <span class="alert-message">{{$errors->first('no_permintaan')}}</span>
                                                                @endif
                                                            </div>
                                                            <div class="form-group col-md-4">
                                                                <label for="tgl_permintaan">Tanggal Permintaan<sup style="color: red">*</sup></label>
                                                                <input class="form-control @error('tgl_permintaan') is-invalid @enderror" id="tgl_permintaan" name="tgl_permintaan" type="text" onblur=validate() onchange=validate() value="{{ date('m-d-Y') }}"placeholder="Masukan tgl_permintaan"required>
                                                                @if($errors->has('tgl_permintaan'))
                                                                    <span class="alert-message">{{$errors->first('tgl_permintaan')}}</span>
                                                                @endif
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="table-responsive">
                                                                <table class="table table-hover table-striped table-bordered datatable" >  
                                                                    <thead>
                                                                        <tr>
                                                                            <th>Barang</th>
                                                                            <th>Lokasi</th>
                                                                            <th>Tersedia</th>
                                                                            <th>Kuantiti</th>
                                                                            <th>Satuan</th>
                                                                            <th>Keterangan</th>
                                                                            <th>Status</th>
                                                                            <th>Harga</th>
                                                                            <th>Action</th>
                                                                        </tr>
                                                                    </thead> 
                                                                    <tbody id="dynamicTable">   
                                                                        <tr hidden=true> 
                                                                            <td></td>
                                                                            <td></td>
                                                                            <td></td>
                                                                            <td></td>
                                                                            <td></td>
                                                                            <td></td>
                                                                            <td></td>
                                                                        </tr>
                                                                </tbody>    
                                                                <tr>
                                                                    <td colspan="7" align="center">Jumlah</td>
                                                                    <td colspan="2"><input type="text" id="jum" value="0" disabled> </td>
                                                                </tr>
                                                                </table>
                                                            </div>
                                                        </div>    
                                                    </div>
                                                </div>
                                                <div class="card-body">
                                                    <div class="row">
                                                        <div class="col-md-12 text-md-right">
                                                            <button type="button" style="float: right;" name="add" id="add" class="btn btn-success btn-sm"><i class="icon-plus"></i>  Tambah</button>
                                                        </div>
                                                    </div>
                                                    <div class="row"> 
                                                        <div class="col-md-12 text-md-center">
                                                            <button type="submit" style="float: center;" class="btn btn-primary btn-sm" disabled="disabled" id="save_me" onclick="return confirm('Apakah Anda Yakin Data Yang Diinputkan Benar ? Jika Benar Klik Ok')">Proses </button>
                                                            <button type="button" style="float: center;" class="btn btn-secondary btn-sm" data-dismiss="modal">Close</button>
                                                        </div>
                                                    </div>
                                                </div>
                                                            
                                            </form>    
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Modal detail  -->
                            <div class="modal fade cd-example-modal-xl" id="myModalDetail" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog modal-xl modal-primary">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel"><i class="icon-user-follow"></i> Detail Permintaan Barang</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <div class="row">
                                                <button type="button" class="btn btn-md btn-primary" style="float:left;margin-left:0.5em" id="select_print">Export Excel <span class="glyphicon glyphicon-file" style="margin-right:0.5em;"></span></button>
                                                <br>
                                                <br>
                                            </div>
                                            <div class="row" style="margin-top: 20px;">
                                                <div class="table-responsive">
                                                    <div class="col-md-12">
                                                        <table class="table table-bordered table-hover datatable-basic" width="100%" id="detailBarang" >
                                                            <thead class="bg-light text-capitalize">
                                                                <tr>
                                                                    <th style="width: 5%">No</th>
                                                                    <th>No Permintaan Barang</th>
                                                                    <th>Nik</th>
                                                                    <th>Nama</th>
                                                                    <th>Departemen </th>
                                                                    <th>Tanggal Permintaan</th>
                                                                    <th>Nama Barang</th>
                                                                    <th>Lokasi</th>
                                                                    <th>Tersedia</th>
                                                                    <th>Kuantiti</th>
                                                                    <th>Satuan</th>
                                                                    <th>Keterangan</th>
                                                                    <th>Harga</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                            </tbody>
                                                            <tr>
                                                                <td colspan="12" align="center">Jumlah</td>
                                                                <td><input type="text" id="jum_detail" value="0" disabled> </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- overview area end -->
    </div>
@endsection
@section('javascript')
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/8.11.8/sweetalert2.all.min.js"></script> 
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/css/bootstrap-datepicker.css" rel="stylesheet">
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.js"></script>
<script src="https://cdn.datatables.net/buttons/2.2.3/js/dataTables.buttons.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdn.datatables.net/buttons/2.2.3/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/2.2.3/js/buttons.print.min.js"></script>

@if( $massage = session('success'))
    <script type="text/javascript">
    Swal.fire({
        type: 'success',
        title:  "{{ $massage }}",
        text: 'success',
        timer: 4000,
    })
    </script>   
@endif
@if ( $massage = session('gagal'))
    <script type="text/javascript">
    Swal.fire({
        type: 'error',
        title:  "{{ $massage }}",
        text: 'Gagal',
        timer: 4000,
    })
@endif 

<script type="text/javascript">
    $(document).ready(function () {
        var table = $('#dt-table').DataTable({
            responsive: true,
            processing: true,
            serverSide: true,
            language: {
                processing: '<span>Harap Menunggu...<i class="fa fa-spinner fa-spin fa-fw"></i></span>',
                lengthMenu: "_MENU_ data per halaman",
                sSearch: "Cari:",
                info : "Total data : _MAX_",
            },
            ajax: "{{ url('PermintaanBarangDataTable')}}",
            columns: [
                { "data": null,"sortable": false, 
                    render: function (data, type, row, meta) {
                    return meta.row + meta.settings._iDisplayStart + 1;
                    }  
                },
                {data: 'no_permintaan', name: 'no_permintaan'},
                {data: 'no_ktp', name: 'no_ktp'},
                {data: 'nama', name: 'nama'},
                {data: 'departemen', name: 'departemen'},
                {data: 'tgl_permintaan', name: 'tgl_permintaan'},
                {data: 'total', name: 'total',render: $.fn.dataTable.render.number( ',', '.', 2 )},
                {data: 'action', name: 'action', orderable: false, searchable: false},
            ]
        });

        $("#tgl").datepicker({
            changeMonth : false,
            changeMonth: true,
            changeYear: true,
            showButtonPanel: true,
            calendarWeeks: true,
            autoclose: true,
            todayHighlight: true,
            onClose: function(dateText, inst) { 
                var day = $("#ui-datepicker-div .ui-datepicker-day :selected").val();
                var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
                var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
                $(this).datepicker('setDate', new Date(year, month,day, 6));
            }
        });
        $('#formDetailAdd').on('submit', (function (e) {
			e.preventDefault();
		
			// $('#btn_simpan_eorderlabdet_add').attr('disabled', 'disabled');
				
			$.ajax({
				url: $(this).attr('action'),
				type: "POST",
				data: new FormData(this),
				dataType: 'json',
				contentType: false,
				cache: false,
				processData: false,
				success: function (data) {
					// $('#btn-simpan').removeAttr('disabled');
					
					if (data.status == "error") {
						Swal.fire({
                            type: 'error',
                            title:  "Tidak Bisa Disimpan",
                            text: data.message,
                            timer: 4000,
                        })
						
					} else {
						$('#formDetailAdd').trigger("reset");
                        $('#modal_add').modal('hide');
						Swal.fire({
                            type: 'success',
                            title:  "success!",
                            text: data.message,
                            timer: 4000,
                        })
                        $('#dt-table').DataTable().ajax.reload(null, false);
                        window.location.reload();
					}

				},
				error: function () {
                    Swal.fire({
                        type: 'error',
                        title:  "Tidak Bisa Disimpan",
                        text: 'Belum Terpenuhi',
                        timer: 4000,
                    })
				}
			});

		}));

    })
    var status_detail = 0
    $(document).on('keyup', ".price",function () {
        var total = 0;
        if(!isNaN(total) && total.length!=0){
                $('.price').each(function(){
                    var vl = this.value.split(',').join('');
                    total += parseFloat(vl);
                }) 
                console.log('total',total);
            }
        console.log('total',total)
        document.getElementById('jum').value=total.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
    })      
    function cekStok(x) {
        var stok  = document.getElementById('stok' + x).value;
        var kuantiti  = document.getElementById('kuantiti' + x).value;
        if(stok){
            const cek_stok= stok - kuantiti;
            document.getElementById('status' + x).value = stok - kuantiti;
            if(cek_stok >=0){
                $('#add').removeAttr('disabled');
                $('#save_me').removeAttr('disabled');
                $("#status_view"+x ).html("<div class='alert alert-success' role='alert'>Terpenuhi</div>");
            }else{
                $("#add").attr("disabled", "disabled");
                $("#save_me").attr("disabled", "disabled");
                $("#status_view"+x ).html("<div class='alert alert-warning' role='alert'>Belum Terpenuhi</div>");
                Swal.fire({
                    type: 'warning',
                    title:  "Stok Habis",
                    text: 'Belum Terpenuhi',
                    timer: 4000,
                })
            }

            var total = 0;
            if(!isNaN(total) && total.length!=0){
                $('.price').each(function(){
                    var vl = this.value.split(',').join('');
                    total += parseFloat(vl);
                }) 
                console.log('total',total);
            }
            console.log('total',total)
            document.getElementById('jum').value=total.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
            
        }
    }
    function sum(x) {
        var nom  = document.getElementById('harga_barang' + x).value;
            if(nom==""){
                nom=0
            }
        document.getElementById('harga_barang' + x).value = nom.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
        //nom.toLocaleString('en-US', {maximumFractionDigits:2});    
    }
    function sum1(x) {
        var nom  = document.getElementById('harga_barang' + x).value;
        if(nom==""){
                nom=0 
            }
        document.getElementById('harga_barang' + x).value = nom.toString().replaceAll(",", "");
    //nom.toLocaleString('en-US', {maximumFractionDigits:2});      
    }

    function hapusdetail(){
        status_detail -= 1;
        console.log('status' + status_detail)
        validate();
    }

    jQuery("#add").on("click", function () {
        validate();
    });

    function validate(){
        jQuery("input[type='text']").each(function(){

            console.log("step 1");
            if (jQuery(this).val() != "" )
            {
                console.log("step 2");
            if(($("#select-nik").val()!="") && ($("#select-kode-barang").val()!="")&&(status_detail > 0) && ($("#kuantiti").val()!== null)&& ($("#keterangan").val()!== null))
            {
                console.log("step 3");
                jQuery("#save_me").removeAttr("disabled"); 
            }
            else {
                console.log("step 4");
                jQuery("#save_me").attr("disabled", "disabled");
            }
            } 
        });
    }



    $('#tgl_permintaan').datepicker( {
        changeMonth: true,
        changeYear: true,
        showButtonPanel: true,
        dateFormat: 'yy-mm-dd',
        
    });

    var i = 0;
       
    $("#add").click(function(e){
        ++i;  
      
        status_detail += 1;
        console.log('status' + status_detail)

        validate();
           $("#dynamicTable").append(`
           <tr>
                <td>
                    <div class="form-group">
                        <select class="select2 form-control" data-fouc id="select-kode-barang`+ i +`"
                            required>
                            <option value="">--Pilih Barang--</option>
                            @foreach ($MasterBarang as $item)
                                <option value="{{ $item['kode_barang']}},{{ $item['lokasi_barang']}},{{ $item['stok']}},{{ $item['satuan']}},{{ $item['harga_barang']}}" >{{ $item['kode_barang']}} - {{ $item['nama_barang']}}</option>
                            @endforeach
                        </select>
                        <input type="hidden" id="kode_barang`+ i +`" name="retribusi['`+i+`'][kode_barang_input]">
                        @if($errors->has('kode_barang'))
                            <span class="alert-message">{{$errors->first('kode_barang')}}</span>
                        @endif
                    </div>
                </td> 
                <td>
                    <div class="form-group">
                        <input class="form-control @error('lokasi_barang') is-invalid @enderror" readonly id="lokasi_barang`+ i +`" name="retribusi['`+i+`'][lokasi_barang]" type="text" placeholder="Masukan Lokasi" value="{{old('lokasi_barang')}}"required>
                        @if($errors->has('lokasi_barang'))
                            <span class="alert-message">{{$errors->first('lokasi_barang')}}</span>
                        @endif
                    </div>
                </td> 
                <td>
                    <div class="form-group">
                        <input class="form-control @error('stok') is-invalid @enderror" readonly id="stok`+ i +`"  name="retribusi['`+i+`'][stok]" type="text" placeholder="Masukan stok" value="{{old('stok')}}"required>
                        @if($errors->has('stok'))
                            <span class="alert-message">{{$errors->first('stok')}}</span>
                        @endif
                    </div>
                </td> 
                <td>
                    <div class="form-group">
                        <input class="form-control" onblur='cekStok(`+i+`)' id="kuantiti`+ i +`" name="retribusi['`+i+`'][kuantiti]" type="number" placeholder="Masukan kuantiti" value="{{old('kuantiti')}}"required>
                        @if($errors->has('kuantiti'))
                            <span class="alert-message">{{$errors->first('kuantiti')}}</span>
                        @endif
                    </div>
                </td> 
                
                <td>
                    <div class="form-group">
                        <input class="form-control @error('satuan') is-invalid @enderror" readonly id="satuan`+ i +`"  name="retribusi['`+i+`'][satuan]" type="text" placeholder="Masukan satuan" value="{{old('satuan')}}"required>
                        @if($errors->has('satuan'))
                            <span class="alert-message">{{$errors->first('satuan')}}</span>
                        @endif
                    </div>
                </td> 
                <td>
                    <div class="form-group">
                        <input class="form-control @error('keterangan') is-invalid @enderror" id="keterangan`+ i +`" name="retribusi['`+i+`'][keterangan]" type="text" placeholder="Masukan keterangan" value="{{old('keterangan')}}"required>
                        @if($errors->has('keterangan'))
                            <span class="alert-message">{{$errors->first('keterangan')}}</span>
                        @endif
                    </div>
                </td> 
                <td>
                    <div class="form-group">
                    <div id="status_view`+ i +`" class="table-responsive"></div>
                        <input class="form-control @error('status') is-invalid @enderror" readonly id="status`+ i +`"  name="retribusi['`+i+`'][status]" type="hidden" placeholder="Masukan status" value="{{old('status')}}"required>
                        @if($errors->has('status'))
                            <span class="alert-message">{{$errors->first('status')}}</span>
                        @endif
                    </div>
                </td> 
                <td>
                    <div class="form-group">
                    <input class="form-control price" id="harga_barang`+i+`" name="retribusi['`+i+`'][harga_barang]" onfocus='sum1(`+i+`)' onblur='sum(`+i+`)' value="0"required>
                    
                    @if($errors->has('harga_barang'))
                        <span class="alert-message">{{$errors->first('harga_barang')}}</span>
                    @endif
                    </div>
                </td>               
                <td><button type="button" id="remove_detail" onclick='hapusdetail()' class="btn btn-danger remove-tr" >Remove</button></td>
            </tr>`
        );

        $("#select-kode-barang"+i).select2({
            width: '100%',
            allowClear: true,
            dropdownCssClass: "bigdrop",
            minimumInputLength: 0,
				language: {
					inputTooShort: function() {
						return '--Pilih--';
					}
				},
        })
        $('#select-kode-barang'+i).on("select2:select", function(e) {
           const dataSplit = e.params.data.id.split(",", 5);
           $('#kode_barang'+i).val($.trim(dataSplit[0]));
            $('#lokasi_barang'+i).val($.trim(dataSplit[1]));
		    $('#stok'+i).val($.trim(dataSplit[2]));
		    $('#satuan'+i).val($.trim(dataSplit[3]));
		    $('#harga_barang'+i).val($.trim(dataSplit[4]));
        });
    });
      
      
    $(document).on('click', '.remove-tr', function(){  
        $(this).parents('tr').remove();
    }); 

    $('#btn-add').on('click', function () {
        $("#select-nik").select2({
            width: '100%',
            allowClear: true,
            dropdownCssClass: "bigdrop",
            minimumInputLength: 0,
				language: {
					inputTooShort: function() {
						return '--Pilih--';
					}
				},
        })
        $('#nama').val();
		$('#departemen').val();
        $('#select-nik').on("select2:select", function(e) {
           const dataSplit = e.params.data.id.split(",", 3);
           $('#no_ktp').val($.trim(dataSplit[0]));
            $('#nama').val($.trim(dataSplit[1]));
		    $('#departemen').val($.trim(dataSplit[2]));
        });
      
        $('#modal_add').modal('show')
    });
    $('#btn-simpan').on('click', function () {
        var status = false
        var data = document.getElementsByClassName('form-input')
        for (let index = 0; index < data.length; index++) {
            if (data[index].value == '') {
                status = true
                data[index].classList.add('border-danger')
            } else {
                status = false
                data[index].classList.remove('border-danger')
            }
        }

        var select = document.getElementsByClassName('form-input-select')
        if (select[0].value == 0) {
            status = true
            document.getElementById('span-level').style.display = 'block'
        } else {
            status = false
            document.getElementById('span-level').style.display = 'none'
        }

        if (status == false) {
            $('#form-add').submit();
        }
    });
    $('body').on('click', '.getDetail', function () {
    // function getDetail(event) {
        // $('#detailBarang').columns.adjust().draw();
        let no_permintaan = $(this).data('id');
        let no_ktp = $(this).data('no_ktp');
        $('#myModalDetail').modal('show');
        var table = $('#detailBarang').DataTable({
            destroy: true,
            responsive: true,
            processing: true,
            serverSide: true,
            language: {
                processing: '<span>Harap Menunggu...<i class="fa fa-spinner fa-spin fa-fw"></i></span>',
                lengthMenu: "_MENU_ data per halaman",
                sSearch: "Cari:",
                info : "Total data : _MAX_",
            },
            ajax: {
                    "url": "{{ url('PermintaanBarangDetail')}}",  
                    "data": {
                        no_permintaan: no_permintaan,
                        no_ktp: no_ktp,
                    },
                },
            columns: [
                { "data": null,"sortable": false, 
                    render: function (data, type, row, meta) {
                    return meta.row + meta.settings._iDisplayStart + 1;
                    }  
                },
                {data: 'no_permintaan', name: 'no_permintaan'},
                {data: 'no_ktp', name: 'no_ktp'},
                {data: 'nama', name: 'nama'},
                {data: 'departemen', name: 'departemen'},
                {data: 'tgl_permintaan', name: 'tgl_permintaan'},
                {data: 'nama_barang', name: 'nama_barang'},
                {data: 'lokasi_barang', name: 'lokasi_barang'},
                {data: 'stok', name: 'stok'},
                {data: 'kuantiti', name: 'kuantiti'},
                {data: 'satuan', name: 'satuan'},
                {data: 'keterangan', name: 'keterangan'},
                {data: 'harga_barang', name: 'harga_barang',render: $.fn.dataTable.render.number( ',', '.', 2 )},
            ],
            buttons: [{
                        extend:'excelHtml5', 
                        text: 'Laporan Detail Permintaan Barang',
                        exportOptions: {
                            columns: [0,1,2,3,4,5,6,7,8,9,10,11,12],
                            modifier: {
                                search: 'applied',
                                order: 'applied'
                            }
                        },
                        title: 'Detail Permintaan Barang',
                        customize: function ( xlsx ) {
                            var sheet = xlsx.xl.worksheets['sheet1.xml'];
                            $('row c[r^="A"]', sheet).attr( 's', '50', ); //<-- left aligned text
                            $('row c[r^="B"]', sheet).attr( 's', '55' ); //<-- wrapped text
                            // align right
                            $('row c[r^="C"]', sheet).attr( 's', '52' );
                            $('row c[r^="D"]', sheet).attr( 's', '52' );
                            $('row c[r^="E"]', sheet).attr( 's', '52' );
                            $('row c[r^="F"]', sheet).attr( 's', '52' );
                            $('row c[r^="G"]', sheet).attr( 's', '52' );
                            $('row c[r^="H"]', sheet).attr( 's', '52' );
                            $('row c[r^="I"]', sheet).attr( 's', '52' );
                            $('row c[r^="J"]', sheet).attr( 's', '52' );
                            $('row c[r^="K"]', sheet).attr( 's', '55' ); //<-- wrapped text
                        } 
                    }]
        });
        $("#select_print").on("click", function() {
                table.button( '.buttons-excel' ).trigger();
        })
        $.ajax({
			dataType: 'json',
			url: "{{ url('totalPermintaanBarangDetail')}}",
            data:{
                no_permintaan: no_permintaan,
                no_ktp: no_ktp,
            },
			success: function (result) {
                console.log(result[0]['total'])
				if(result[0]){
					if (result[0].total) {
						$('#jum_detail').val(rubah(result[0]['total']));
					}
				}
				
				
			}

		});
    })
    function rubah(angka) {
        var reverse = angka.toString().split('').reverse().join(''),
            ribuan = reverse.match(/\d{1,3}/g);
        ribuan = ribuan.join('.').split('').reverse().join('');
        return ribuan;
    }
</script>
@endsection