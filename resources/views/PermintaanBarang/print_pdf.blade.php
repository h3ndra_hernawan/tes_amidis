<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    
    <title>Laporan Retribusi</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<style>
    .page {
        width: 21cm;
        min-height: 29.7cm;
        padding-left: 2cm;
        padding-top: 2cm;
        padding-right: 2cm;
        padding-bottom: 2cm;
    }
    @page {
        size: A4 portrait;
        margin-left: 2cm;
        margin-top: 2cm;
        margin-right: 2cm;
        margin-bottom: 2cm;
    }
    table{
        border:1px solid #333;
        border-collapse:collapse;
        margin:0 auto;
    
    }
    td, tr, th{
        padding: 12px;
        font-size: 8pt;
        border:1px solid #333;

    }
    p{
        font-size: 9pt;
    }
    th{
        background-color: #f0f0f0;
    }
    h4, p{
        margin:0px;
    }
    @media screen {
       
       { display: none;}
    
    }
    @media print {
    
        table { page-break-after:auto }
        tr    { page-break-inside:avoid; page-break-after:auto }
        td    { page-break-inside:avoid; page-break-after:auto }
        thead { display:table-header-group }
        tfoot { display:table-footer-group }
                
    }

    @page {
      margin: 20px 30px 40px 50px;
    }
</style>
</head>
<body>  
      
    <table style="white-space: normal">
        <thead class="bg-light text-capitalize">
            <tr>
                <td colspan="9" align="center" style="float: center;">  <h1>LAPORAN PERMINTAAN BARANG</h1></td>
            </tr>
            <tr>
                <th style="width: 5%">No</th>
                <th>No Permintaan Barang</th>
                <th>Nik</th>
                <th>Nama</th>
                <th>Departemen </th>
                <th>Tanggal Permintaan</th>
                <th>Nama Barang</th>
                <th>Kuantiti</th>
                <th>Harga</th>
            </tr>
        </thead>
        <tbody>
            @php
            $counter = 0
            @endphp
            @foreach ($DataDetail as $item)
            <tr>
                <td>{{ $counter += 1 }}</td>
                <td>{{ $item['no_permintaan'] }}</td>
                <td>{{ $item['no_ktp'] }}</td>
                <td>{{ $item['nama'] }}</td>
                <td>{{ $item['departemen'] }}</td>
                <td>{{date('d F Y', strtotime($item['tgl_permintaan']))  }}</td>
                <td>{{ $item['nama_barang'] }}</td>
                <td>{{ $item['kuantiti'] }}</td>
                <td>Rp.{{number_format($item['harga_barang'], 2, ',', '.') }}</td>
            
            </tr>
            @endforeach
        </tbody>
        <tr>
            <td colspan="8" align="center">Jumlah</td>
            <td>Rp.{{number_format($DataHeader['total'], 2, ',', '.') }}</td>
        </tr>
    </table>    
</body>
</html>