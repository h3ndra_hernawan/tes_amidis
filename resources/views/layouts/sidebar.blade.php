<div class="sidebar-menu">
    <div class="sidebar-header">
        <div class="logo">
            @if (session()->get('level') == 1)
                <h4 style="color:white;">Admin</h4>
            @else
                <h4 style="color:white;">User </h4>
            @endif
        </div>
    </div>
    <!-- class="active" -->
    <div class="main-menu">
        <div class="menu-inner">
            <nav>
                <ul class="metismenu" id="menu">
                    @if (session()->get('level') == 1)
                        <li class="{{$page=='dasboard' ? 'active' : '' }} ">
                            <a href="javascript:void(0)" aria-expanded="true"><i class="ti-user"></i><span>Dashboard</span></a>
                            <ul class="collapse">
                                <li class="{{ $page=='dasboard' ? 'active' : '' }} "><a href="{{ url('/dashboard') }}"><i class="ti-dashboard"></i><span>dashboard</span></a></li>
                            </ul>    
                        </li>

                        <li class="{{ $page=='user' || $page=='MasterBarang' || $page=='MasterKaryawan' ? 'active' : '' }} ">
                            <a href="javascript:void(0)" aria-expanded="true"><i class="ti-layout-sidebar-left"></i><span>Master</span>
                            </a>
                            <ul class="collapse">
                                <li class="{{ $page=='user'? 'active' : '' }} "><a href="{{ url('/user') }}">User</a></li>
                            </ul>
                            <ul class="collapse">
                                <li class="{{ $page=='MasterBarang'? 'active' : '' }} "><a href="{{ url('/MasterBarang') }}">Master Barang</a></li>
                            </ul>
                            <ul class="collapse">
                                <li class="{{ $page=='MasterKaryawan'? 'active' : '' }} "><a href="{{ url('/MasterKaryawan') }}">Master Karyawan</a></li>
                            </ul>
                        </li>
                        <li class="{{$page=='PermintaanBarang' ? 'active' : '' }} ">
                            <a href="javascript:void(0)" aria-expanded="true"><i class="ti-user"></i><span>Permintaan Barang</span></a>
                            <ul class="collapse">
                                <li class="{{$page=='PermintaanBarang' ? 'active' : '' }} "><a href="{{ url('/PermintaanBarang') }}">Permintaan Barang</a></li>
                            </ul>
                        </li>
                    @else
                    <li class="{{$page=='PermintaanBarang' ? 'active' : '' }} ">
                        <a href="javascript:void(0)" aria-expanded="true"><i class="ti-user"></i><span>Permintaan Barang</span></a>
                        <ul class="collapse">
                            <li class="{{$page=='PermintaanBarang' ? 'active' : '' }} "><a href="{{ url('/PermintaanBarang') }}">Permintaan Barang</a></li>
                        </ul>
                    </li>
                    @endif
                </ul>
            </nav>
        </div>
    </div>
</div>