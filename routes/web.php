<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\NotificationController;


Route::group(['prefix' => '/' , 'middleware' => 'guest'], function () {
    Route::get('/', 'LoginController@index')->name('/');
    Route::post('/login.check_login', 'LoginController@postlogin')->name('login.check_login');  
    Route::post('/login.user_daftar', 'LoginController@daftarUser')->name('login.user_daftar'); 
  
}); 
Route::group(['prefix'=>'/','middleware' => 'check'], function () {
    Route::post('logout' , '\App\Http\Controllers\Auth\LoginController@logout')->name('logout');
    Route::get('/dashboard', 'DashboardController@index')->name('/dashboard');

    // Master 
    Route::get('/user', 'UserController@index')->name('/user');
    Route::get('/MasterBarang', 'MasterBarangController@index')->name('/MasterBarang');
    Route::get('/MasterKaryawan', 'MasterKaryawanController@index')->name('/MasterKaryawan');

    // Print pdf
    Route::get('/data/print/{id}','PermintaanBarangController@print_pdf')->name('/print_pdf');

    // route buat combo
    Route::get('/data/masterbarang', 'PermintaanBarangController@masterbarang');

    // datatable
    Route::get("PermintaanBarangDataTable", "PermintaanBarangController@getPermintaanBarang");
    Route::get("PermintaanBarangDetail", "PermintaanBarangController@getPermintaanBarangDetail");
    Route::get("totalPermintaanBarangDetail", "PermintaanBarangController@gettotalPermintaanBarangDetail");


    Route::resource('user', 'UserController');
    Route::resource('MasterBarang', 'MasterBarangController');
    Route::resource('MasterKaryawan', 'MasterKaryawanController');
    Route::resource('PermintaanBarang', 'PermintaanBarangController');
}); 
